import React, { Component } from "react";

class Form extends Component {
  state = {
    input: "",
  };
  handleChange = e => {
    this.setState({
      input: e.target.value,
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.addTask(this.state);
    this.setState({
      input: ""
    });
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          onChange={this.handleChange}
          value={this.state.input}
          placeholder="enter a task"
        />

        <button
          className="btn waves-effect waves-light"
          type="submit"
          name="action"
        >
          Add
        </button>
      </form>
    );
  }
}

export default Form;

import React from "react"

const Tasks = ({tasks}) => {
    return (
<div>
    {tasks.map((task, index) => (
        <div key={index}>
        <span>{task.input}</span> 
        </div>
    ))}
</div>
    )
}

export default Tasks
import React, { Component } from "react";
import Form from "./components/form";
import Tasks from "./components/tasks";
import "./App.css";

class App extends Component {
  state = {
    tasks: [],
  };
  addTask = task => {
    task.id = Math.random();
    let tasks = [...this.state.tasks, task];
    this.setState({
      tasks,
    });
  };
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h1>Todo's</h1>
          <p>Made with React</p>

          <Form addTask={this.addTask} />
          <Tasks tasks={this.state.tasks} />
        </div>
      </div>
    );
  }
}

export default App;
